# Aluma .NET SDK

This repository is where we develop the .NET SDK libraries for [Aluma](https://aluma.io/).
The .NET SDK targets .NET Standard 2.0. We explicitly support .NET Core 2.1 and
higher, as well as .NET Framework 4.7.2 and higher. The SDK _should_ work on
Mono version 5.4 and higher, but this is not tested and support is on a best-
effort basis only.

If you are using Visual Studio, please ensure you are using Visual Studio 2017
or later.

## Installing the SDK

The Aluma .NET SDK is available as a package. You can install it in the usual
way, detailed below for stable builds and early releases. [The documentation](https://docs.Aluma.io)
has information to help you get started, and you will need to obtain an API client ID and
secret from [the Aluma portal](https://app.aluma.io/developers).

Builds are available from the public NuGet feed, and can be installed using one of the following
mechanisms, as best suits your application.

### Package Manager
```powershell
Install-Package Aluma.Http
```

### dotnet CLI
```powershell
dotnet add package Aluma.Http
```

### `PackageReference` project file element
```xml
<PackageReference Include="Aluma.Http" Version="2.0.0" />
```

## Sample projects
Sample projects are provided to illustrate how to use the library. We recommend starting with these.
 
You can find the projects in the Aluma.NET.sln and the samples folder:
 * OCR. Illustrates use of the client to list projects and OCR a set of files.
 * ExportData. Illustrates use of the client to list tasks that are pending export and export data from those tasks.
 
