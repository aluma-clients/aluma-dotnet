﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Aluma.Http.Samples.ExportData
{
    internal class Program
    {
        private static string GetMandatoryEnvironmentVariable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            var value = Environment.GetEnvironmentVariable(name);
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException($"The required environment variable '{name}' is not set.");
            }

            return value;
        }

        public static async Task Main(string[] args)
        {
            var client = AlumaClient.Create(GetMandatoryEnvironmentVariable("ALUMA_API_KEY"),
                GetMandatoryEnvironmentVariable("ALUMA_API_SECRET"));

            // Get the project used for this sample, which is identical to the standard demo project but with validation disabled.
            var project = await GetOrCreateProject(client);
            
            // Create a task from a sample PDF
            await using var fs = File.OpenRead("input/sample-document-small.pdf");
            var newTask = await client.CreateTaskAsync(fs, project.Id, "application/pdf", "Sample document (small)");
            Console.WriteLine($"Created task {newTask.Id}");

            // Loop continuously, polling every 5 seconds for tasks that are ready for export
            while (true)
            {
                Console.WriteLine("Checking for tasks that are pending export...");

                foreach (var task in await client.ListTasksAsync(new TaskStatus(TaskState.PendingStartClientAction,
                             TaskClientAction.Export)))
                {
                    await ExportTask(client, task);
                }

                Console.WriteLine("Done");
                Thread.Sleep(5000);
            }
        }

        private static async Task<Project> GetOrCreateProject(AlumaClient client)
        {
            var project = await client.GetProjectAsync(".net client library - export data sample project");
            if (project != null)
            {
                return project;
            }

            // The project is not already present, so upload it now
            var projectFile = "project/Invoices_and_Delivery_Notes_v2_No_Validation.alproj";
            project = await client.UploadProjectAsync(projectFile);
            Console.WriteLine($"Uploaded project file {projectFile}");
            return project;
        }

        private static async Task ExportTask(AlumaClient client, AlumaTask task)
        {
            try
            {
                Console.WriteLine($"Exporting {task.Id} {task.Status.State} {task.Status.ClientAction}...");

                // Start the client action to indicate that export is starting, and get the export specification that provides all
                // the information required to do the export.
                var exportSpec =
                    await client.StartExportAsync(task.Id, TimeSpan.FromSeconds(30))
                        .ConfigureAwait(false);

                // A task can contain one or more documents
                var docSpecs = exportSpec.Documents.ToArray();
                for (var index = 0; index < docSpecs.Length; index++)
                {
                    var documentSpec = docSpecs[index];
                    Console.WriteLine($"Data for document {index+1} of {docSpecs.Length}:");
                    
                    // There can be one or more files to export for each document. The names and content of the files are defined in the project.
                    foreach (var fileSpec in documentSpec.Files)
                    {
                        // This project includes a JSON, CSV and original input export files. We just process the JSON here though.  
                        if (!fileSpec.Filename.EndsWith(".json")) continue;

                        // Create a stream into which the content of the export file will be copied
                        using var stream = new MemoryStream();
                        // Pass the file specification and stream to the client
                        await client.GetExportFileContentAsync(fileSpec, stream);
                        // Use a StreamReader to read the content from the stream, because we know this export file content is text
                        using var reader = new StreamReader(stream);
                        string json = await reader.ReadToEndAsync();
                        
                        // Parse the text as JSON and write the data to the console
                        var data = JObject.Parse(json ?? throw new InvalidOperationException());
                        WriteDataToConsole(data);
                    }
                }

                // Complete the client action to indicate that the export has been successful and the Aluma task can be completed & cleaned up.
                await client.CompleteExportAsync(task.Id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Either cancel or fail the client action.
                // If the action is cancelled, the task status will be reset back to "PendingStartClientAction (export)" immediately. If you do this,
                // you should implement a mechanism to wait for some appropriate time before exporting the task again. One simple way to do this is to
                // implement a time-expiring cache of task IDs, which you add to here and check before doing the StartClientAction request.
                // If the action is failed, the task status will be set to Failed and no further processing will take place.  
                await client.CancelExportAsync(task.Id);
                // await client.FailExportAsync(task.Id, "error message");
            }
        }
        
        private static void WriteDataToConsole(JObject jsonObject)
        {
            foreach (var property in jsonObject.Properties())
            {
                var name = property.Name;
                var value = property.Value;

                switch (value)
                {
                    case JObject nestedObject:
                    {
                        // Ignore export metadata
                        if (name == "metadata") continue;

                        // A lookup field is an object with a set of properties, e.g. 
                        // "Employee": {
                        //     "ID": "00001",
                        //     "Location": "London",
                        //     "StartDate": "01-01-2023"
                        // }
                        Console.WriteLine($"Field: {name} (lookup)");
                        foreach (var p in nestedObject.Properties())
                        {
                            Console.WriteLine($"  {p.Name}: {p.Value}");
                        }

                        break;
                    }
                    case JArray array:
                    {
                        // A table field is an array with one object per row, and within that object
                        // one property per column
                        Console.WriteLine($"Field: {name} (table)");
                        for (var index = 0; index < array.Count; index++)
                        {
                            var row = array[index];
                            var r = row as JObject;
                            foreach (var column in r.Properties())
                            {
                                Console.WriteLine($"Row {index + 1} {column.Name}: {column.Value}");
                            }
                        }

                        break;
                    }
                    default:
                        // A text, date, list or search field is a basic JSON property and value.
                        Console.WriteLine($"{name}: {value}");
                        break;
                }
            }
        }
    }
}