﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Aluma.Http.Samples.OCR
{
    internal class Program
    {
        private static string GetMandatoryEnvironmentVariable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }
            var value = Environment.GetEnvironmentVariable(name);
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException($"The required environment variable '{name}' is not set.");
            }
            return value;
        }

        public static async Task Main(string[] args)
        {
            var client = AlumaClient.Create(GetMandatoryEnvironmentVariable("ALUMA_API_KEY"), GetMandatoryEnvironmentVariable("ALUMA_API_SECRET"));

            // Uncomment for console trace logging
            // Logging.LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());
            
            // --------------------------------
            // List all projects in the account
            // --------------------------------
            foreach (var project in await client.ListProjectsAsync())
            {
                Console.WriteLine($"{project.Name}\n\tId: {project.Id}\n\tCreated On: {project.CreationTime}\n\tLast Modified: {project.LastModifiedTime}\n");
            }

            // ---------------------
            // OCR a folder of files
            // ---------------------
            // There either needs to be a project called 'OCR' already present in the account, or a project file available locally to upload (see below)
            var inputFolder = "input";
            var outputFolder = "output";
            var ocrProject = await client.GetProjectAsync("OCR");
            if (ocrProject == null)
            {
                // The project is not already present, so upload it now
                var projectFile = "project/Packaged/OCR.alproj";
                ocrProject = await client.UploadProjectAsync(projectFile);
                Console.WriteLine($"Uploaded project file {projectFile}");
                Console.WriteLine($"Project name is '{ocrProject.Name}'");
            }

            var taskTimeout = TimeSpan.FromSeconds(120); // The maximum allowed time for a task to complete
            var maxParallelTasks = 100; // Limit how many tasks we're creating and running in parallel
            var semaphore = new SemaphoreSlim(maxParallelTasks);
            var tasks = new List<Task<TaskExportResult?>>();
            foreach (var inputFile in Directory.EnumerateFiles(inputFolder, "*.pdf"))
            {
                await semaphore.WaitAsync();
                var task = Task.Run(async () =>
                {
                    try
                    {
                        var alumaTask = await client.CreateTaskAsync(inputFile, ocrProject.Id);
                        var success = await client.AwaitTaskStatusAsync(alumaTask.Id, new TaskStatus(TaskState.PendingStartClientAction, TaskClientAction.Export), taskTimeout);
                        if (!success)
                        {
                            // Update to latest status
                            alumaTask = await client.GetTaskAsync(alumaTask.Id);
                            Console.WriteLine(
                                $"ERROR: The task {alumaTask.Name} did not complete in the allotted time of {taskTimeout.TotalSeconds}s. Task status is '{alumaTask.Status}'.");
                            return null;
                        }
                        
                        var exportResult = await client.PerformExportAsync(alumaTask, outputFolder);
                        
                        if (exportResult == null) return exportResult;
                        
                        foreach (var doc in exportResult.Documents)
                        {
                            foreach (var file in doc.Files)
                            {
                                if (file.IsSuccess)
                                {
                                    string folder = Path.Combine(Environment.CurrentDirectory, outputFolder);
                                    Console.WriteLine(
                                        $"File {Path.Combine(file.Folder, file.Filename)} successfully written to {folder}");
                                }
                                else
                                {
                                    Console.WriteLine(
                                        $"ERROR: File {Path.Combine(file.Folder, file.Filename)} was not exported: {file.Error?.Message ?? "Unknown error"}");
                                }
                            }
                        }

                        return exportResult;
                    }
                    finally
                    {
                        semaphore.Release(); // Release the semaphore slot
                    }
                });
                tasks.Add(task);
            }
            // Wait for all tasks to complete
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }
    }
}