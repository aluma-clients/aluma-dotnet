﻿Create sub-folders in the Document Types folder, one for each distinct document type. The name of the folder must match the
document type output in the document separation and/or type identification steps. Each sub-folder should have a FPXL file defining
the extraction for the document type, and optionally a Settings.json file for validation and export settings (although this will be
auto-created when the project is packaged if it doesn't already exist).