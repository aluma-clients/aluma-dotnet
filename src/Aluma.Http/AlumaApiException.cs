using System;

namespace Aluma.Http
{
    /// <inheritdoc />
    /// <summary>
    /// Represents an error occurring in calling the Aluma API
    /// </summary>
    public class AlumaApiException : Exception
    {
        /// <inheritdoc />
        public AlumaApiException()
        {
        }

        /// <inheritdoc />
        public AlumaApiException(string message) : base(message)
        {
        }

        /// <inheritdoc />
        public AlumaApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}