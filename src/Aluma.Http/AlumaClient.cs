﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aluma.Http.RequestHandling;
using Aluma.Http.Responses;
using Newtonsoft.Json;

[assembly: InternalsVisibleTo("Aluma.Http.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: InternalsVisibleTo("Aluma.Pipelines")]

namespace Aluma.Http
{
    /// <summary>
    /// The top-level client class for communicating with the Aluma API.
    /// </summary>
    public sealed class AlumaClient
    {
        internal const string DefaultUrl = "https://api.aluma.io";

        private readonly IHttpRequestSender _requestSender;
        private static readonly Dictionary<string, string> JsonRequestHeaders = new Dictionary<string, string>() { { "accept", "application/json" } };

        /// <summary>
        /// Creates a new instance of <see cref="AlumaClient"/> using the given
        /// API client credentials.
        /// <param name="clientId">The Aluma API Client ID to use when authenticating with the service.</param>
        /// <param name="clientSecret">The Aluma API Client Secret to use when authenticating with the
        /// service.</param>
        /// <param name="apiUri">Optional. The Uri of a self-hosted Aluma deployment.</param>
        /// <returns>A new <see cref="AlumaClient"/> instance ready for use.
        /// </returns>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// public static class Program
        /// {
        ///     public static async Task Main(string[] args)
        ///     {
        ///         var client = AlumaClient.Create("my-client-id", "my-client-secret");
        ///         foreach (var project in await client.ListProjectsAsync())
        ///         {
        ///             await Console.WriteLineAsync(project.Id);
        ///         }
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>
        /// </summary>
        public static AlumaClient Create(string clientId, string clientSecret, Uri apiUri = null)
        {
            apiUri = apiUri ?? new Uri(DefaultUrl);

            var requestSender = new LoggingRequestSender(
                new TimeoutHandlingRequestSender(
                    new FailedRequestHandlingRequestSender(
                        new ReliableRequestSender(new RequestSender(
                            new HttpClient
                            {
                                BaseAddress = apiUri
                            })))));

            var accessTokenService = new AccessTokenService(
                clientId, clientSecret,
                requestSender);

            return new AlumaClient(new TokenFetchingRequestSender(accessTokenService, requestSender));
        }

        internal AlumaClient(IHttpRequestSender requestSender)
        {
            _requestSender = requestSender;
        }

        /// <summary>
        /// Gets or sets a duration on the underlying <see cref="HttpClient"/>
        /// to wait until the requests time out. The timeout unit is seconds, and defaults to 120.
        /// </summary>
        /// <seealso cref="HttpClient.Timeout"/>
        public int Timeout
        {
            get => _requestSender.Timeout;
            set => _requestSender.Timeout = value;
        }

        /// <summary>
        /// Creates a new task in the Aluma platform from the provided <see cref="Stream"/>.
        /// </summary>
        /// <param name="taskSource">The <see cref="Stream"/> source of the task.</param>
        /// <param name="projectId">The unique ID of the project to assign this task to.</param>
        /// <param name="contentType">The MIME type of the stream, either application/pdf or image/tiff.</param>
        /// <param name="taskName">An optional name for the new task.</param>
        /// <param name="fileName">The original name of the underlying file, which will be made available as metadata.</param>
        /// <param name="filePath">The original path of the underlying file, which will be made available as metadata.</param>
        /// <param name="fileCreatedTime">The original creation time of the underlying file, which will be made available as metadata.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>An <see cref="AlumaTask"/> for the new task.</returns>
        public async Task<AlumaTask> CreateTaskAsync(Stream taskSource,
            string projectId, string contentType = "application/pdf", string taskName = null, string fileName = null,
            string filePath = null, DateTime? fileCreatedTime = null, CancellationToken cancellationToken = default)
        {
            if (taskSource == null)
            {
                throw new ArgumentNullException(nameof(taskSource));
            }

            if (projectId == null)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            if (!taskSource.CanSeek)
            {
                // This Stream implementation from ASP.NET Core buffers streams larger than the specified threshold to
                // a temporary file, up to the buffer limit. Furthermore, it is a seekable Stream, so we can query the
                // Stream's length
                taskSource = new FileBufferingReadStream(
                    taskSource,
                    1024 * 1024, /* 1MiB */
                    30 * 1000 * 1000, /* 30 MB */
                    Path.GetTempPath);

                await taskSource.ReadAsync(new byte[1], 0, 1, cancellationToken).ConfigureAwait(false);
                taskSource.Seek(0, SeekOrigin.Begin);
            }

            if (taskSource.Length < 1)
            {
                throw new ArgumentException("The provided stream has no content.", nameof(taskSource));
            }

            // Build tags for optional info about the task
            var tags = new List<string>();
            if (!string.IsNullOrEmpty(taskName)) tags.Add($"task_name={Uri.EscapeDataString(taskName)}");
            if (!string.IsNullOrEmpty(fileName)) tags.Add($"file_name={Uri.EscapeDataString(fileName)}");
            if (!string.IsNullOrEmpty(filePath)) tags.Add($"file_path={Uri.EscapeDataString(filePath)}");
            // Either use the provided creation time, or default to now
            long timestamp;
            if (fileCreatedTime.HasValue)
            {
                timestamp = (new DateTimeOffset(fileCreatedTime.Value.ToUniversalTime())).ToUnixTimeSeconds();
            }
            else
            {
                timestamp = (new DateTimeOffset(DateTime.UtcNow)).ToUnixTimeSeconds();
            }
            tags.Add($"file_created_time={timestamp}");

            var request =
                new HttpRequestMessageTemplate(HttpMethod.Post,
                    new Uri($"/tasks?project_id={projectId}", UriKind.Relative), JsonRequestHeaders)
                {
                    Content = new StreamContent(taskSource)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue(contentType)
                        }
                    }
                };
            request.Headers.Add("X-Aluma-Tags", string.Join("&", tags));
            var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
            var task = await response.Content.ReadAsAsync<TaskResponse>().ConfigureAwait(false);

            return new AlumaTask(task);
        }

        /// <summary>
        /// Creates a new task in the Aluma platform from the provided local file.
        /// </summary>
        /// <param name="path">A path to a file on disk from which the document will be created.</param>
        /// <param name="projectId">The unique ID of the project to assign this task to.</param>
        /// <param name="taskName">An optional name for the task. If null, the name will default to the input filename. Use an empty string for no task name.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>An <see cref="AlumaTask"/> for the new task.</returns>
        public async Task<AlumaTask> CreateTaskAsync(string path, string projectId, string taskName = null,
            CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(path));
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"The file '{path}' could not be found.");
            }

            string contentType;
            switch (Path.GetExtension(path).ToLowerInvariant())
            {
                case "tiff":
                case "tif":
                    contentType = "image/tiff";
                    break;
                default:
                    contentType = "application/pdf";
                    break;
            }

            taskName = taskName ?? Path.GetFileName(path);

            using (FileStream fileStream = File.OpenRead(path))
            {
                return await CreateTaskAsync(fileStream, projectId, contentType, taskName, Path.GetFileName(path),
                    Path.GetDirectoryName(path), File.GetCreationTime(path), cancellationToken).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Fetch a reference for the given task in the Aluma platform.
        /// </summary>
        /// <param name="id">The ID of the task.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>An <see cref="AlumaTask"/> for the specified task ID.</returns>
        public async Task<AlumaTask> GetTaskAsync(string id, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(id));
            }

            var request = new HttpRequestMessageTemplate(HttpMethod.Get, new Uri($"/tasks/{id}", UriKind.Relative), JsonRequestHeaders);

            var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);

            var responseContent = await response.Content.ReadAsAsync<TaskResponse>().ConfigureAwait(false);
            return new AlumaTask(responseContent);
        }

        /// <summary>
        /// List all tasks in the account on the Aluma platform.
        /// </summary>
        /// <param name="filterStatus">A task status to filter tasks by. Only tasks with this status will be listed and returned.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>An array of <see cref="AlumaTask"/> objects.</returns>
        public async Task<AlumaTask[]> ListTasksAsync(TaskStatus filterStatus = null, CancellationToken cancellationToken = default)
        {
            var url = "/tasks";
            if (filterStatus != null)
            {
                url += $"?state={filterStatus.StateJson}";

                if (filterStatus.ClientAction != TaskClientAction.None)
                {
                    url += $"&client_action={filterStatus.ClientActionJson}";    
                }
            }
            
            var request = new HttpRequestMessageTemplate(HttpMethod.Get, new Uri(url, UriKind.Relative), JsonRequestHeaders);

            var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
            var responseContent = await response.Content.ReadAsAsync<ListTasksResponse>().ConfigureAwait(false);
            return responseContent.Tasks.Select(p => new AlumaTask(p)).ToArray();
        }
        
        /// <summary>
        /// Cancels this task, preventing it from undergoing additional processing.
        /// </summary>
        public async Task CancelTaskAsync(string taskId, CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessageTemplate(HttpMethod.Put, new Uri($"/tasks/{taskId}/cancel", UriKind.Relative));

            await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Starts export of data or output files for a task, by sending a Start Client Action (export). The returned <see cref="ExportSpecification"/>
        /// should be used to access data or get the URLs for files that should be downloaded. You should call CompleteExportAsync to
        /// indicate that the export has been successful and the task can be completed. Alternatively:
        /// - call CancelExportAsync to set the task status back to pending export (and implement a mechanism to ensure you wait an appropriate time
        /// before calling this method again), or
        /// - call FailExportAsync to indicate that the export can not be completed and the task status should be set to Failed. 
        /// </summary>
        /// <param name="taskId">The ID of the task.</param>
        /// <param name="timeout">The maximum amount of time requested from the service for the download to complete. Beyond this the service will set the task
        /// status back to pending export. The default value is 5 minutes.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is <see cref="CancellationToken.None"/>.</param>
        /// <returns>An <see cref="ExportSpecification"/> containing the data and/or download URLs for the task.</returns>
        public async Task<ExportSpecification> StartExportAsync(string taskId, TimeSpan? timeout = null, CancellationToken cancellationToken = default)
        {
            // Default value for timeout if not set
            timeout ??= TimeSpan.FromMinutes(5);

            var request = new HttpRequestMessageTemplate(HttpMethod.Put, new Uri($"/tasks/{taskId}/start_client_action?action=export&timeout={timeout.Value.TotalSeconds}", UriKind.Relative));

            var startActionResponse = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
            var exportResponse = await startActionResponse.Content.ReadAsAsync<ExportResponse>().ConfigureAwait(false);
            var exportSpec = new ExportSpecification(exportResponse.Documents?.Select(d =>
                {
                    return new ExportDocumentSpecification(d.Files?.Select(f => new ExportFileSpecification(
                        f.Filename,
                        f.Folder,
                        f.Source == ExportFileSource.Content ? ExportSpecificationFileSource.Content : ExportSpecificationFileSource.Url, f.Content, f.Url)).ToArray());
                }).ToArray());

            return exportSpec;
        }
        
        /// <summary>
        /// Completes an export started using StartExportAsync and indicates that export was successful and the task can be completed.
        /// </summary>
        /// <param name="taskId">The ID of the task.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="CancellationToken.None"/>.</param>
        public async Task CompleteExportAsync(string taskId, CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessageTemplate(HttpMethod.Put, new Uri($"/tasks/{taskId}/complete_client_action?action=export", UriKind.Relative));

            await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Cancels an export started using StartExportAsync and sets the task status back to pending export. If you call this method, for example if
        /// a system is not available to receive the exported data, you should consider implementing a mechanism to wait an appropriate time before
        /// calling StartExportAsync again.
        /// </summary>
        /// <param name="taskId">The ID of the task.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="CancellationToken.None"/>.</param>
        public async Task CancelExportAsync(string taskId, CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessageTemplate(HttpMethod.Put, new Uri($"/tasks/{taskId}/cancel_client_action?action=export", UriKind.Relative));

            await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Fails an export started using StartExportAsync and indicates that the export can not be completed and the task status should be set to Failed.
        /// </summary>
        /// <param name="taskId">The ID of the task.</param>
        /// <param name="errorMessage">Details of the error, to store with the failed task.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="CancellationToken.None"/>.</param>
        public async Task FailExportAsync(string taskId, string errorMessage, CancellationToken cancellationToken = default)
        {
            var errorBody = new
            {
                message = errorMessage
            };
            
            var request = new HttpRequestMessageTemplate(HttpMethod.Put, new Uri($"/tasks/{taskId}/fail_client_action?action=export", UriKind.Relative), JsonRequestHeaders)
            {
                Content = new StringContent(JsonConvert.SerializeObject(errorBody), Encoding.UTF8, "application/json")
            };
            await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the content for an export file and copies it to a stream.
        /// </summary>
        /// <param name="fileSpec">An <see cref="ExportFileSpecification"/> retrieved from the <see cref="ExportSpecification"/> returned by <see cref="StartExportAsync"/>.</param>
        /// <param name="stream">A writable stream into which the content will be copied. The stream's position will be reset to the beginning if it supports seeking.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="CancellationToken.None"/>.</param>
        public async Task GetExportFileContentAsync(ExportFileSpecification fileSpec, Stream stream, CancellationToken cancellationToken = default)
        {
            if (fileSpec == null) throw new ArgumentNullException(nameof(fileSpec));
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            if (!stream.CanWrite) throw new ArgumentException("The stream must be writable", nameof(stream));

            switch (fileSpec.Source)
            {
                case ExportSpecificationFileSource.Content:
                    // Get the content from the fileSpec
                    var writer = new StreamWriter(stream);
                    await writer.WriteAsync(fileSpec.Content).ConfigureAwait(false);
                    await writer.FlushAsync().ConfigureAwait(false); // Ensure all data is written to the stream

                    // Reset the stream's position to the beginning so it can be read by the consumer
                    if (stream.CanSeek)
                    {
                        stream.Position = 0;
                    }

                    break;
 
                case ExportSpecificationFileSource.Url:
                    // Make a request to get the content
                    var request =
                        new HttpRequestMessageTemplate(HttpMethod.Get, new Uri(fileSpec.Url, UriKind.Relative));
                    var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);

                    // Get the content stream from the response
                    var contentStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);

                    try
                    {
                        // Copy content from the response stream to the stream given to u
                        await contentStream.CopyToAsync(stream, 81920, cancellationToken).ConfigureAwait(false);
                        
                        // Reset the stream's position to the beginning so it can be read by the consumer
                        if (stream.CanSeek)
                        {
                            stream.Position = 0;
                        }
                    }
                    finally
                    {
                        // Dispose the content stream explicitly
                        if (contentStream is IDisposable disposableStream)
                        {
                            disposableStream.Dispose();
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Waits for the task to reach a specific status
        /// </summary>
        /// <param name="taskId">The ID of the task to wait for.</param>
        /// <param name="desiredStatus">The specific status to wait for.</param>
        /// <param name="maximumWaitTime">The maximum amount of time to wait before giving up.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>True if the desired state was reached within the allotted time, and False otherwise.</returns>
        public async Task<bool> AwaitTaskStatusAsync(string taskId, TaskStatus desiredStatus, TimeSpan maximumWaitTime,
            CancellationToken cancellationToken = default)
        {
            if (desiredStatus == null)
            {
                throw new ArgumentNullException(nameof(desiredStatus));
            }

            string statusQueryParams = $"&desired_state={desiredStatus.StateJson}";
            if (desiredStatus.ClientAction != TaskClientAction.None)
            {
                statusQueryParams += $"&desired_client_action={desiredStatus.ClientActionJson}";
            }

            DateTime startTime = DateTime.UtcNow;
            // Maximum time allowed by request
            TimeSpan maxPollTime = TimeSpan.FromSeconds(30);
            do
            {
                // Calculate the remaining time
                TimeSpan remainingTime = maximumWaitTime - (DateTime.UtcNow - startTime);
                // Determine the poll duration (min of maxPollTime or remainingTime)
                TimeSpan pollDuration = remainingTime < maxPollTime ? remainingTime : maxPollTime;
                int pollSeconds = (int)pollDuration.TotalSeconds;
                // Round up values less than 1 to avoid an error
                pollSeconds = pollSeconds < 1 ? 1 : pollSeconds;

                var request = new HttpRequestMessageTemplate(HttpMethod.Get,
                    new Uri($"/tasks/{taskId}?wait={pollSeconds}{statusQueryParams}", UriKind.Relative), JsonRequestHeaders);
                var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
                var taskResponse = await response.Content.ReadAsAsync<TaskResponse>().ConfigureAwait(false);
                // Check the task state
                var updatedTask = new AlumaTask(taskResponse);
                // Return success if the task is now in the desired state
                if (updatedTask.Status.Equals(desiredStatus)) return true;
                // Break out of the loop if the task is in an 'end state', as we don't expect it to change further.
                if (updatedTask.Status.IsEndState) break;
            } while (DateTime.UtcNow - startTime < maximumWaitTime);

            // Desired state not reached within the allotted time
            return false;
        }

        /// <summary>
        /// Perform the export client action for a task, and download all exported files.
        /// </summary>
        /// <param name="alumaTask">The task to export from.</param>
        /// <param name="exportDirectory">A local folder into which to write the files (note that subfolders may be created if the export specifies this).
        /// If this is null or empty, the files will instead be returned as byte arrays within the <see cref="TaskExportResult"/>.</param>
        /// <param name="timeout">The maximum amount of time requested from the service for the download to complete. Beyond this the service will set the task
        /// status back to pending export.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>A <see cref="TaskExportResult"/> giving details of all the downloaded files.</returns>
        /// <exception cref="InvalidOperationException">Thrown if the task is not ready for export.</exception>
        public async Task<TaskExportResult> PerformExportAsync(AlumaTask alumaTask, string exportDirectory, TimeSpan? timeout = null, CancellationToken cancellationToken = default)
        {
            if (alumaTask == null) throw new ArgumentNullException(nameof(alumaTask));
            
            // Check task is in the right state
            if (alumaTask.Status.State != TaskState.PendingStartClientAction || alumaTask.Status.ClientAction != TaskClientAction.Export)
            {
                // Final live check
                var currentTask = await GetTaskAsync(alumaTask.Id, cancellationToken).ConfigureAwait(false);
                if (currentTask.Status.State != TaskState.PendingStartClientAction || currentTask.Status.ClientAction != TaskClientAction.Export)
                {
                    throw new InvalidOperationException($"Task {alumaTask.Id} is not ready for export. Its current status is '{currentTask.Status}'");
                }
            }

            var exportSpec = await StartExportAsync(alumaTask.Id, timeout, cancellationToken).ConfigureAwait(false);
            
            int maxParallelDocs = 20; // Limit how many documents will be exported at once
            var semaphore = new SemaphoreSlim(maxParallelDocs);
            var documentTasks = new List<Task<DocumentExportResult>>();
            foreach (var documentSpec in exportSpec.Documents)
            {
                await semaphore.WaitAsync(cancellationToken).ConfigureAwait(false);
                // Download the files for each document in a new task and release the semaphore when done
                var task = Task.Run(async () =>
                {
                    try
                    {
                        return await DownloadFilesAsync(documentSpec, exportDirectory, cancellationToken).ConfigureAwait(false);
                    }
                    finally
                    {
                        semaphore.Release(); // Release the semaphore slot
                    }
                }, cancellationToken);

                documentTasks.Add(task);
            }
            // Wait for all tasks to complete
            var documentResults = await Task.WhenAll(documentTasks).ConfigureAwait(false);
            var taskResult = new TaskExportResult(documentResults);
            // Check whether everything succeeded
            if (taskResult.IsSuccess)
            {
                await CompleteExportAsync(alumaTask.Id, cancellationToken).ConfigureAwait(false);
            }
            else
            {
                await FailExportAsync(alumaTask.Id, $"Export failed with error '{taskResult.AllErrors.FirstOrDefault()}'", cancellationToken).ConfigureAwait(false);
            }
            return taskResult;
        }

        private async Task<DocumentExportResult> DownloadFilesAsync(ExportDocumentSpecification docSpecification, string exportDirectory, CancellationToken cancellationToken)
        {
            var documentExportResult = new DocumentExportResult();
            foreach (var fileSpec in docSpecification?.Files ?? Enumerable.Empty<ExportFileSpecification>())
            {
                var fileExportResult = new FileExportResult();
                documentExportResult.Files.Add(fileExportResult);
                try
                {
                    fileExportResult.Filename = fileSpec.Filename;
                    fileExportResult.Folder = PathNormalizer.NormalizePath(fileSpec.Folder);
                    string outputFile = null;
                    // We have to manage two possible input formats (a content string or a Uri to download from), and two possible outputs (a byte array or a file on disk)
                    if (exportDirectory != null)
                    {
                        string outputFolder = Path.Combine(exportDirectory == string.Empty ? Environment.CurrentDirectory : exportDirectory, fileExportResult.Folder);
                        // Ensure the destination directory exists
                        if (!Directory.Exists(outputFolder))
                        {
                            Directory.CreateDirectory(outputFolder);
                        }
                        outputFile = Path.Combine(outputFolder, fileSpec.Filename);
                    }
                    switch (fileSpec.Source)
                    {
                        case ExportSpecificationFileSource.Content:
                            if (outputFile == null)
                            {
                                // Export to byte array
                                fileExportResult.FileContents = Encoding.UTF8.GetBytes(fileSpec.Content);
                            }
                            else
                            {
                                // Export to file on disk
                                
                                // The output filename can itself have a directory encoded in it, for example where
                                // the desired output is one folder per document.
                                var outputFileFolder = Path.GetDirectoryName(outputFile);
                                if (!Directory.Exists(outputFileFolder))
                                {
                                    Directory.CreateDirectory(outputFileFolder);
                                }
                                File.WriteAllText(outputFile, fileSpec.Content, Encoding.UTF8);
                            }
                            break;
                        case ExportSpecificationFileSource.Url:
                            Stream outputStream;
                            if (outputFile == null)
                            {
                                // Export to byte array
                                outputStream = new MemoryStream();
                            }
                            else
                            {
                                // Export to file on disk
                                outputStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None);
                            }

                            // Download file content into the output stream
                            await GetExportFileContentAsync(fileSpec, outputStream, cancellationToken)
                                .ConfigureAwait(false);
                            
                            if (outputFile == null)
                            {
                                fileExportResult.FileContents = (outputStream as MemoryStream).ToArray();
                            }
                            else
                            {
                                fileExportResult.DownloadedFilePath = outputFile;
                            }
                            outputStream.Close();
                            outputStream.Dispose();
                            break;
                    }
                }
                catch (TaskCanceledException)
                {
                    fileExportResult.Error = new OperationCanceledException();
                }
                catch (HttpRequestException ex)
                {
                    fileExportResult.Error = ex;
                }
                catch (IOException ex)
                {
                    fileExportResult.Error = ex;
                }
                catch (UnauthorizedAccessException ex)
                {
                    fileExportResult.Error = ex;
                }
            }
            return documentExportResult;
        }

        /// <summary>
        /// Get all projects in the account on the Aluma platform.
        /// </summary>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>An array of <see cref="Project"/> objects.</returns>
        public async Task<Project[]> ListProjectsAsync(CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessageTemplate(HttpMethod.Get, new Uri($"/projects", UriKind.Relative), JsonRequestHeaders);
            var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
            var responseContent = await response.Content.ReadAsAsync<ListProjectsResponse>().ConfigureAwait(false);
            return responseContent.Projects.Select(p => new Project(p)).ToArray();
        }
        
        /// <summary>
        /// Deletes this project from the Aluma platform.
        /// </summary>
        public async Task DeleteProjectAsync(string projectId ,CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessageTemplate(HttpMethod.Delete, new Uri($"/projects/{projectId}", UriKind.Relative));

            await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Fetch a reference for the given project in the Aluma platform.
        /// </summary>
        /// <param name="name">The name of the project.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>A <see cref="Project"/> for the specified project, or null if no project with that name exists.</returns>
        public async Task<Project> GetProjectAsync(string name, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
            }

            var request = new HttpRequestMessageTemplate(HttpMethod.Get, new Uri($"/projects", UriKind.Relative))
            {
                Headers =
                {
                    { "accept", "application/json" }
                }
            };

            var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);

            var responseContent = await response.Content.ReadAsAsync<ListProjectsResponse>().ConfigureAwait(false);
            ProjectResponse requestedProject = responseContent.Projects.FirstOrDefault(p =>
                p.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            if (requestedProject == null)
            {
                return null;
            }
            else
            {
                return new Project(requestedProject);
            }
        }

        /// <summary>
        /// Upload a new or updated project to the Aluma platform from a stream.
        /// </summary>
        /// <param name="projectSource">The <see cref="Stream"/> source of the project file.</param>
        /// <param name="allowUpdate">Indicates whether to allow updating an existing project with the same name.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>A <see cref="Project"/> for the new/updated project.</returns>
        public async Task<Project> UploadProjectAsync(Stream projectSource, bool allowUpdate = false,
            CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessageTemplate(HttpMethod.Post,
                new Uri($"/projects?update={allowUpdate.ToString().ToLower()}", UriKind.Relative), JsonRequestHeaders)
            {
                Content = new StreamContent(projectSource)
                {
                    Headers =
                    {
                        ContentType = new MediaTypeHeaderValue("application/zip")
                    }
                }
            };

            var response = await _requestSender.SendAsync(request, cancellationToken).ConfigureAwait(false);
            var responseContent = await response.Content.ReadAsAsync<ProjectResponse>().ConfigureAwait(false);
            return new Project(responseContent);
        }

        /// <summary>
        /// Upload a new or updated project to the Aluma platform from disk.
        /// </summary>
        /// <param name="path">The path to the project file (.alproj).</param>
        /// <param name="allowUpdate">Indicates whether to allow updating an existing project with the same name.</param>
        /// <param name="cancellationToken">
        /// The token to monitor for cancellation requests. The default value is
        /// <see cref="CancellationToken.None"/>.
        /// </param>
        /// <returns>A <see cref="Project"/> for the new/updated project.</returns>
        public async Task<Project> UploadProjectAsync(string path, bool allowUpdate = false,
            CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(path));
            }

            using (FileStream fileStream = File.OpenRead(path))
            {
                return await UploadProjectAsync(fileStream, allowUpdate, cancellationToken).ConfigureAwait(false);
            }
        }
    }
}