﻿using System;
using Aluma.Http.Responses;

namespace Aluma.Http
{
    /// <summary>
    /// The current state of an Aluma Task. Instances can be obtained
    /// via task operations on <see cref="AlumaClient"/>.
    /// </summary>
    public class AlumaTask
    {
        /// <summary>
        /// Gets the string identifier of this task in the Aluma platform.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Gets the name of this task, if set.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the string identifier of the project this task is associated with.
        /// </summary>
        public string ProjectId { get; }

        /// <summary>
        /// Gets the name of the project this task is associated with.
        /// </summary>
        public string ProjectName { get; }

        /// <summary>
        /// Gets the string identifier of the file collection underlying this task.
        /// </summary>
        public string FileCollectionId { get; }

        /// <summary>
        /// Gets the time at which the task was created.
        /// </summary>
        public DateTime CreationTime { get; }

        /// <summary>
        /// Gets the number of documents within the task, or 0 if that has not yet been assessed.
        /// </summary>
        public int DocumentsCount { get; }

        /// <summary>
        /// The status of the task at the time this object was created. Use <see cref="AlumaClient.GetTaskAsync"/>
        /// to get an up-to-date status.
        /// </summary>
        public TaskStatus Status { get; }

        internal AlumaTask(TaskResponse response)
        {
            Id = response.Id;
            Name = response.Name;
            Status = new TaskStatus(response.State, response.ClientAction, response.ValidationInstance);
            ProjectId = response.ProjectId;
            ProjectName = response.ProjectName;
            CreationTime = response.CreatedAt;
            FileCollectionId = response.FileCollectionId;
        }
    }
}