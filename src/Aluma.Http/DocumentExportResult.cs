using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Aluma.Http;

/// <summary>
/// The results for a single document within a <see cref="TaskExportResult"/>.
/// </summary>
public class DocumentExportResult
{
    /// <summary>
    /// A list of files exported for this document.
    /// </summary>
    public IList<FileExportResult> Files { get; }

    /// <summary>
    /// A flag indicating wither the export for this document was 100% successful. If False, the individual <see cref="FileExportResult.Error"/>
    /// properties should be checked for more details.
    /// </summary>
    public bool IsSuccess => Files.All(f => f != null && f.IsSuccess);

    /// <summary>
    /// Get all exception messages that occurred during the export of this document.
    /// </summary>
    public IEnumerable<string> AllErrors => Files.Where(f => f?.Error?.Message != null).Select(f => f.Error.Message);

    /// <summary>
    /// Create a new empty DocumentExportResult.
    /// </summary>
    public DocumentExportResult()
    {
        Files = new List<FileExportResult>();
    }
}