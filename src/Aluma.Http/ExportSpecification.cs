using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Aluma.Http;

/// <summary>
/// Contains details of the output files for each document in a task.
/// </summary>
public class ExportSpecification
{
    public ExportSpecification(IEnumerable<ExportDocumentSpecification> documents)
    {
        Documents = documents;
    }

    /// <summary>
    /// The export details for each document in a task.
    /// </summary>
    public IEnumerable<ExportDocumentSpecification> Documents { get; }
}

public class ExportDocumentSpecification
{
    public ExportDocumentSpecification(IEnumerable<ExportFileSpecification> files)
    {
        Files = files;
    }

    /// <summary>
    /// The export files for a single document in the task. The number of files and the
    /// content, filename and folder of each are defined in the Aluma project used to create
    /// the task.
    /// </summary>
    public IEnumerable<ExportFileSpecification> Files { get; }
}

public enum ExportSpecificationFileSource
{
    Content,
    Url
}

/// <summary>
/// Represents a single output file for a document in a task.
/// The content, filename and folder of each output file are defined in the Aluma project used to create
/// the task. You should pass this object as a parameter to <see cref="GetExportFileContentAsync"/> along
/// with a stream into which the content will be copied.
/// <example>
/// <code>
/// // There are one or more documents in a task
/// foreach (var documentSpec in exportSpec.Documents)
/// {
///     // There are zero or more files to export for each document.
///     // The filenames, folder names and content of the files are defined in the project.
///     foreach (var fileSpec in documentSpec.Files)
///    {
///         using var stream = new MemoryStream();
///         await client.GetExportFileContentAsync(fileSpec, stream);
///
///         using var reader = new StreamReader(stream);
///         string content = await reader.ReadToEndAsync();
///         ProcessContent(content);
///     }
/// }
/// </code>
/// </example>
/// </summary>
[SuppressMessage("Design", "CA1056:Uri properties should not be strings")]
[SuppressMessage("Design", "CA1054:Uri parameters should not be strings")]
public class ExportFileSpecification
{
    public ExportFileSpecification(string filename, string folder, ExportSpecificationFileSource source, string content, string url)
    {
        Filename = filename;
        Folder = folder;
        Source = source;
        Content = content;
        Url = url;
    }

    /// <summary>
    /// The name that should be given to the export file, as defined in the project. If the filename is defined with any field
    /// value placeholders then these will have been replaced with values. 
    /// </summary>
    public string Filename { get; }
    
    /// <summary>
    /// The folder that the file should be exported to, as defined in the project.  If the folder is defined with any field
    /// value placeholders then these will have been replaced with values.
    /// </summary>
    public string Folder { get; }
    
    internal ExportSpecificationFileSource Source { get; }
    
    internal string Content { get;  }

    internal string Url { get; }
}
