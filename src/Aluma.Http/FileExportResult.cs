using System;

namespace Aluma.Http;

/// <summary>
/// The result of downloading a single file within a <see cref="DocumentExportResult"/>.
/// </summary>
public class FileExportResult
{
    /// <summary>
    /// The subfolder (as specified within the project configuration) in which the export file should be stored.
    /// </summary>
    public string Folder { get; set; }

    /// <summary>
    /// The target filename of the export file.
    /// </summary>
    public string Filename { get; set; }

#pragma warning disable CA1819 // We're OK with a property returning an array here
    /// <summary>
    /// The byte contents of the file (if no local file folder was specified).
    /// </summary>
    public byte[] FileContents { get; set; }
#pragma warning restore CA1819
    /// <summary>
    /// The full path to the downloaded file (if a local file folder was specified).
    /// </summary>
    public string DownloadedFilePath { get; set; }

    /// <summary>
    /// An exception which occurred during download.
    /// </summary>
    public Exception Error { get; set; }

    /// <summary>
    /// A flag indicating whether the export of this file was successful.
    /// </summary>
    public bool IsSuccess => Error == null && (FileContents != null || DownloadedFilePath != null);
}