﻿using System;
using System.IO;

namespace Aluma.Http
{
    internal static class PathNormalizer
    {
        /// <summary>
        /// Normalize any directory separator characters in a path name to be appropriate for the current OS
        /// </summary>
        /// <param name="path">The input path.</param>
        /// <returns>The normalized path name.</returns>
        public static string NormalizePath(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            // Replace alternative directory separator with the current OS's separator
            return path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar)
                       .Replace(Path.DirectorySeparatorChar == '/' ? '\\' : '/', Path.DirectorySeparatorChar);
        }
    }
}
