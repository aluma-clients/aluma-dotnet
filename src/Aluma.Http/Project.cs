﻿using System;
using System.Net.Http;
using System.Threading;
using Aluma.Http.RequestHandling;
using Aluma.Http.Responses;

namespace Aluma.Http
{
    /// <summary>
    /// The data associated with a project on the Aluma platform. Instances can be obtained
    /// via project operations on <see cref="AlumaClient"/>.
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Gets the string identifier of this project in the Aluma platform.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Gets the name of this project.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets a description of the project, if set.
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Gets the time at which the project was created.
        /// </summary>
        public DateTime CreationTime { get; }

        /// <summary>
        /// Gets the time at which the project was last modified.
        /// </summary>
        public DateTime LastModifiedTime { get; private set; }

        internal Project(ProjectResponse response)
        {
            Id = response.Id;
            Name = response.Name;
            Description = response.Description;
            CreationTime = response.CreationTime;
            LastModifiedTime = response.LastModifiedTime;
        }


    }
}