﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Aluma.Http.RequestHandling
{
    internal interface IHttpRequestSender
    {
        int Timeout { get; set; }

        Task<HttpResponseMessage> SendAsync(HttpRequestMessageTemplate request, CancellationToken cancellationToken = default);
    }
}