﻿using Newtonsoft.Json;

namespace Aluma.Http.Responses
{
    internal class ApiError
    {
        [JsonProperty]
        internal string Message { get; set; }
    }
}
