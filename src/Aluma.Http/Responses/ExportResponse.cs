﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Aluma.Http.Responses
{
    internal class ExportResponse
    {
        [JsonProperty("documents")]
        public ExportDocumentResponse[] Documents { get; set; }
    }

    internal class ExportDocumentResponse
    {
        [JsonProperty("files")]
        public ExportFileResponse[] Files { get; set; }

    }

    internal enum ExportFileSource
    {
        [EnumMember(Value = "content")]
        Content,
        [EnumMember(Value = "url")]
        Url
    }

    internal class ExportFileResponse
    {
        [JsonProperty("folder")]
        public string Folder { get; set; }
        [JsonProperty("filename")]
        public string Filename { get; set; }
        [JsonProperty("source")]
        public ExportFileSource Source { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
