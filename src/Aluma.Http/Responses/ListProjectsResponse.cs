﻿using Newtonsoft.Json;

namespace Aluma.Http.Responses
{
    internal class ListProjectsResponse
    {
        [JsonProperty("projects")]
        public ProjectResponse[] Projects {get; set; }
    }
}
