using Newtonsoft.Json;

namespace Aluma.Http.Responses;

internal class ListTasksResponse
{
    [JsonProperty("tasks")]
    public TaskResponse[] Tasks {get; set; }

}