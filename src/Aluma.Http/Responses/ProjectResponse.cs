﻿using System;
using Newtonsoft.Json;

namespace Aluma.Http.Responses
{
    internal class ProjectResponse
    {
        [JsonProperty("id")]
        public string Id {get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("file_id")]
        public string FileId { get; set; }

        [JsonProperty("created")]
        public DateTime CreationTime { get; set; }

        [JsonProperty("updated")]
        public DateTime LastModifiedTime { get; set; }

    }
}
