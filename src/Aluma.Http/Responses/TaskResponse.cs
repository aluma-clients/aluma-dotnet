﻿using System;
using Newtonsoft.Json;

namespace Aluma.Http.Responses
{
    internal class TaskResponse
    {
        [JsonProperty("id")]
        public string Id {get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("file_collection_id")]
        public string FileCollectionId { get; set; }

        [JsonProperty("project_id")]
        public string ProjectId { get; set; }

        [JsonProperty("project_name")]
        public string ProjectName { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("documents_count")]
        public int DocumentsCount { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("client_action")]
        public string ClientAction { get; set; }

        [JsonProperty("validation_instance")]
        public string ValidationInstance { get; set; }
    }
}
