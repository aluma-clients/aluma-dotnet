using System.Collections.Generic;
using System.Linq;

namespace Aluma.Http;

/// <summary>
/// The results of a call to <see cref="AlumaClient.DownloadExportFilesAsync"/>.
/// </summary>
public class TaskExportResult
{
    /// <summary>
    /// An enumeration of all documents within the export.
    /// </summary>
    public IEnumerable<DocumentExportResult> Documents { get; }

    /// <summary>
    /// A flag indicating wither the export was 100% successful. If False, the individual <see cref="FileExportResult.Error"/>
    /// properties should be checked for more details.
    /// </summary>
    public bool IsSuccess => Documents.All(d => d != null && d.IsSuccess);

    /// <summary>
    /// Get all exception messages that occurred during the export.
    /// </summary>
    public IEnumerable<string> AllErrors => Documents.SelectMany(d => d == null ? Enumerable.Empty<string>() : d.AllErrors);

    /// <summary>
    /// Create a new TaskExportResult with the supplied <see cref="DocumentExportResult"/>s.
    /// </summary>
    /// <param name="documents"></param>
    public TaskExportResult(IEnumerable<DocumentExportResult> documents)
    {
        Documents = documents;
    }
}