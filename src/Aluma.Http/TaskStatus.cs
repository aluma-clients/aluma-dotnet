﻿using System.Runtime.Serialization;
using System.Reflection;
using System;

namespace Aluma.Http
{
    /// <summary>
    /// An enumeration of potential task states.
    /// </summary>
    public enum TaskState
    {
        [EnumMember(Value = "not-started")] NotStarted,
        [EnumMember(Value = "queued")] Queued,
        [EnumMember(Value = "running")] Running,

        [EnumMember(Value = "pending:start_client_action")]
        PendingStartClientAction,

        [EnumMember(Value = "pending:complete_client_action")]
        PendingCompleteClientAction,
        [EnumMember(Value = "completed")] Completed,
        [EnumMember(Value = "cancelling")] Cancelling,
        [EnumMember(Value = "cancelled")] Cancelled,
        [EnumMember(Value = "timed-out")] TimedOut,
        [EnumMember(Value = "failed")] Failed,
        Unknown
    }

    /// <summary>
    /// An enumeration of potential client actions (used when <see cref="TaskStatus.State"/> is either
    /// <see cref="TaskState.PendingStartClientAction"/> or <see cref="TaskState.PendingCompleteClientAction"/>).
    /// </summary>
    public enum TaskClientAction
    {
        [EnumMember(Value = "")] None,

        [EnumMember(Value = "upload-file-content")]
        UploadFileContent,
        [EnumMember(Value = "validation")] Validation,
        [EnumMember(Value = "export")] Export,
        Unknown
    }

    /// <summary>
    /// A class which combines the elements of a task status.
    /// </summary>
    public class TaskStatus
    {
        /// <summary>
        /// The current state of the task.
        /// </summary>
        public TaskState State { get; }

        /// <summary>
        /// The specific client action awaited if <see cref="State"/> is either
        /// <see cref="TaskState.PendingStartClientAction"/> or <see cref="TaskState.PendingCompleteClientAction"/>).
        /// </summary>
        public TaskClientAction ClientAction { get; }

        /// <summary>
        /// The specific validation instance awaited if <see cref="ClientAction"/> is <see cref="TaskClientAction.Validation"/>.
        /// </summary>
        public string ValidationInstance { get; }

        /// <summary>
        /// Set or get <see cref="State"/> using the equivalent JSON string. The state will be set to <see cref="TaskState.Unknown"/>
        /// if the string is not recognised.
        /// </summary>
        public string StateJson
        {
            get => GetEnumJsonValue(State);
        }

        /// <summary>
        /// Set or get <see cref="ClientAction"/> using the equivalent JSON string. The client action will be set to <see cref="TaskClientAction.Unknown"/>
        /// if the string is not recognised.
        /// </summary>
        public string ClientActionJson
        {
            get => GetEnumJsonValue(ClientAction);
        }

        /// <summary>
        /// Indicates whether the task is in a state which will no longer change over time.
        /// </summary>
        public bool IsEndState
        {
            get
            {
                return State switch
                {
                    TaskState.Cancelled or TaskState.Failed or TaskState.TimedOut or TaskState.Completed => true,
                    _ => false
                };
            }
        }

        /// <summary>
        /// Create a new task status with default values.
        /// </summary>
        public TaskStatus()
        {
            State = TaskState.Unknown;
            ClientAction = TaskClientAction.None;
        }

        /// <summary>
        /// Create a new task status with the specified values.
        /// </summary>
        public TaskStatus(TaskState state, TaskClientAction clientAction = TaskClientAction.None,
            string validationInstance = "") : this()
        {
            State = state;
            ClientAction = clientAction;
            ValidationInstance = validationInstance;
        }
        
        /// <summary>
        /// Create a new task status with the specified values.
        /// </summary>
        public TaskStatus(string state, string clientAction = default,
            string validationInstance = default)
        {
            State = ParseEnum(state, TaskState.Unknown);
            ClientAction = ParseEnum(clientAction, TaskClientAction.Unknown);
            ValidationInstance = validationInstance;
        }

        protected bool Equals(TaskStatus other)
        {
            return other != null && State == other.State && ClientAction == other.ClientAction && ValidationInstance == other.ValidationInstance;
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TaskStatus)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)State;
                hashCode = (hashCode * 397) ^ (int)ClientAction;
                hashCode = (hashCode * 397) ^ (ValidationInstance != null ? ValidationInstance.GetHashCode() : 0);
                return hashCode;
            }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            if (ClientAction == TaskClientAction.None)
            {
                return StateJson;
            }

            if (!string.IsNullOrEmpty(ValidationInstance))
            {
                return $"{StateJson} ({ClientActionJson} - {ValidationInstance})";
            }

            return $"{StateJson} ({ClientActionJson})";
        }

        private static string GetEnumJsonValue(Enum enumValue)
        {
            // Get the type of the enum
            var enumType = enumValue.GetType();

            // Get the enum field info
            var fieldInfo = enumType.GetField(enumValue.ToString());

            // Get the EnumMember attribute (if exists)
            var attribute = (EnumMemberAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(EnumMemberAttribute));

            // Return the value of the EnumMember attribute, or default to the enum name if not present
            return attribute?.Value ?? enumValue.ToString();
        }

        private static T ParseEnum<T>(string value, T defaultValue) where T : Enum
        {
            // Get the type of the enum
            var enumType = typeof(T);

            // Iterate through all the enum fields
            foreach (var field in enumType.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                // Check if the field has the EnumMember attribute
                var attribute = field.GetCustomAttribute<EnumMemberAttribute>();

                // If the EnumMember attribute exists and its Value matches the input string, return the corresponding enum value
                if (attribute != null && attribute.Value == value)
                {
                    return (T)field.GetValue(null);
                }
            }

            // If no match was found, return the supplied default
            return defaultValue;
        }
    }
}