﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Aluma.Http.RequestHandling;
using Newtonsoft.Json;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Xunit;

namespace Aluma.Http.Tests.RequestHandling
{
    public class AccessTokenServiceFacts
    {
        private const string ExpectedClientId = nameof(ExpectedClientId);
        private const string ExpectedClientSecret = nameof(ExpectedClientSecret);

        private readonly IHttpRequestSender _requestSender = Substitute.For<IHttpRequestSender>();
        private readonly AccessTokenService _sut;

        public AccessTokenServiceFacts()
        {
            _sut = new AccessTokenService(
                ExpectedClientId, ExpectedClientSecret,
                _requestSender);
        }

        [Fact]
        public async Task Login_sends_a_request_with_the_specified_credentials()
        {
            _requestSender
                .SendAsync(Arg.Any<HttpRequestMessageTemplate>())
                .Returns(ci => Response.GetToken(ci.Arg<HttpRequestMessageTemplate>()));

            await _sut.FetchAccessTokenAsync();

            await _requestSender
                .Received(1)
                .SendAsync(Arg.Is<HttpRequestMessageTemplate>(m =>
                    m.Method == HttpMethod.Post &&
                    IsJsonWithClientCredentials(m.Content, ExpectedClientId, ExpectedClientSecret)));
        }

        [Fact]
        public async Task Login_throws_if_response_is_not_success_code()
        {
            _requestSender
                .SendAsync(Arg.Any<HttpRequestMessageTemplate>())
                .Throws(new AlumaApiException(Response.ErrorMessage));

            var exception = await Assert.ThrowsAsync<AlumaApiException>(() =>
                _sut.FetchAccessTokenAsync());

            Assert.Equal(Response.ErrorMessage, exception.Message);
        }

        private static bool IsJsonWithClientCredentials(HttpContent content, string expectedClientId,
            string expectedClientSecret)
        {
            if (content is not JsonContent)
            {
                return false;
            }

            var streamReader = new StreamReader(content.ReadAsStream());
            var jsonTextReader = new JsonTextReader(streamReader);
            var data = new JsonSerializer().Deserialize<Dictionary<string, string>>(jsonTextReader);

            Assert.Equal(expectedClientId, data["client_id"]);
            Assert.Equal(expectedClientSecret, data["client_secret"]);
            return true;
        }
    }
}